# final

> This is the final project folder for FirebaseUI Auth for Vue with Facebook, Google and Email Blog

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

https://softauthor.com/firestore-crud-app-vuejs-authentication
https://softauthor.com/firebaseui-vue-login-with-facebook-google-and-email-pasword

https://www.youtube.com/watch?v=sYNjEzcOTOs

https://www.npmjs.com/package/vue-moment

https://firebase.google.com/docs/firestore/query-data/order-limit-data

https://vuejs.org/v2/guide/conditional.html#v-show

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions

https://leightley.com/firebase-functions-oncreate-ondelete/