import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Home from '@/components/Home'
import Profile from '@/components/Profile'
import Recommendations from '@/components/Recommendations'
import About from '@/components/About'
import Friends from '@/components/Friends'
import firebase from 'firebase'

Vue.use(Router)

let router = new Router({
    mode: 'history',

    routes: [{
        path: '/profile',
        name: 'Profile',
        component: Profile,
        meta: {
          auth: true
        }

      },
      {
        path: '/login',
        name: 'Login',
        component: Login,
        meta: {
          guest: true
        }

      },
      {
        path: '/about',
        name: 'About',
        component: About,
        
      },
      {
        path: '/',
        name: 'Home',
        component: Home,
        meta: {
            auth: true
        }
      },
      {
        path: '/friends',
        name: 'Friends',
        component: Friends,
        meta: {
            auth: true
        }
      },
      {
        path: '/recommendations',
        name: 'Recommendations',
        component: Recommendations,
        meta: {
            auth: true
        }
      },
    ]
  },
)


router.beforeEach((to, from, next) => {

  if (to.matched.some(record => record.meta.auth)) {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        next()
      } else {
        next({
          path: "/login",
        })
      }
    })
  } else if (to.matched.some(record => record.meta.guest)) {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        next({
          path: "/profile",
        })
      } else {
        next()
      }
    })

  } else {
    next()
  }

})

export default router
