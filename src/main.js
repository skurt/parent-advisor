// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import firebase from 'firebase'
Vue.config.productionTip = false


const firebaseConfig = {
  apiKey: "AIzaSyDqGsVQ0FqT4uujw-HQUEa13LPTjf1aKDE",
  authDomain: "test-parent-advisor.firebaseapp.com",
  databaseURL: "https://test-parent-advisor.firebaseio.com",
  projectId: "test-parent-advisor",
  storageBucket: "test-parent-advisor.appspot.com",
  messagingSenderId: "1078773921035",
  appId: "1:1078773921035:web:39b2e1cd8f830d90799e0e",
  measurementId: "G-4F21M89L1J"
};
firebase.initializeApp(firebaseConfig);



/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: {
    App
  },

  template: '<App/>'
})
