const functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);
let db = admin.firestore();

exports.createProfile = functions.auth.user().onCreate((user) => {
  return db.collection('users')
            .doc(user.uid)
            .set({
              createdDate: new Date(),
              id: user.uid,
              name: user.displayName,
              photoUrl: user.photoURL,
              friends: [],
            });
});